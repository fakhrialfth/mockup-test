import "./App.css";
import { Routes, Route } from "react-router-dom";
import Homepage from "./Pages/Homepage/Homepage";
import Login from "./Pages/Login/Login";
import InputData from "./Pages/Data/InputData";
import Register from "../src/Pages/Register/Register";

function App() {
  return (
    <>
      <Routes>
        <Route exact path="/" element={<Homepage />} />
        <Route path="login" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route path="data" element={<InputData />} />
      </Routes>
    </>
  );
}
export default App;
