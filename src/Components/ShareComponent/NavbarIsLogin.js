import logo from "../../Assets/Logos/Logo.png";
import style from "../../Styling/ShareComponent/navbarUser.module.css";

export default function UserNavbar() {
  return (
      <nav
        className={`${style.navbar} navbar fixed-top navbar-expand-lg navbar-expand-md navbar-expand-sm mx-auto`}
      >
        <div className={`${style.main_container} container`}>
          <a className={style.left_container} href="/">
            <img src={logo} alt="logo" className={style.logo} />
          </a>
        </div>
      </nav>
  );
}
