import { combineReducers } from "redux";

//Import reducernya disini
import { getAuthRegister } from "./UserReducer";
import { getProfile } from "./ProfileReducer";

const reducers = combineReducers({
  //Masukan Reducer yang telah diimport kesini
   getAuthRegister,
  getProfile,
});

export default reducers;
