import React, { useEffect } from "react";
import {
  Container,
  Nav,
  Navbar,
  Col,
  Row,
} from "react-bootstrap";
import Logo from "../../Assets/Logos/Logo.png";
import SignUp from "../../Assets/Icons/right white.png";
import Laptop from "../../Assets/Logos/Laptop.png";
import style from "../../Styling/Pages/Homepage/Homepage.module.css";
import { getProfile } from "../../Redux/Action/ProfileAction";

import { useDispatch, useSelector } from "react-redux";
export default function Homepage() {
  const image = useSelector((state) => state.getProfile.image);
  const nama = useSelector((state) => state.getProfile.nama);
  const token = useSelector((state) => state.getAuthRegister.token);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getProfile());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <>
      <Navbar className={style.navbarHomepage} variant="white">
        <Container className={style.homepageLeft}>
          <Navbar.Brand>
            <img src={Logo} alt="Logo" />
          </Navbar.Brand>
        </Container>
        {token ? (
          <Container className={style.homepageRight}>
            <img
              className={style.icon}
              src={
                image ||
                "https://res.cloudinary.com/dry2yqm3h/image/upload/v1644199101/image/whiteboard/no-profile-pic_zyup0u.png"
              }
              alt="ProfilePicture"
            />
            <Nav.Link href="/login">{nama}</Nav.Link>
          </Container>
        ) : (
          <Container className={style.homepageRight}>
            <Nav.Link href="/login">Log In</Nav.Link>
            <a href="/register">
              <button className={style.btn_SignUp} variant="SignUp">
                Sign up <img src={SignUp} alt="Button" />
              </button>
            </a>
          </Container>
        )}
      </Navbar>
      <div className={style.hero}>
        <h1 className="text-center">
          Input Your Data <br />
          to join our <span className="text-primary">Company</span>
        </h1>
        <p className="text-center">
          We protect your data, your data will be safe.
        </p>
        {token ? (
          <a href="/data">
            <button className={style.btn_board} variant="board">
              Get Started <img src={SignUp} alt="Button" />
            </button>
          </a>
        ) : (
          <a href="/register">
            <button className={style.btn_board} variant="board">
              Get Started <img src={SignUp} alt="Button" />
            </button>
          </a>
        )}
      </div>
      <div className={style.mainHomepage}>
        <img src={Laptop} alt="Button" />
      </div>
      <div className={style.footer}>
        <Container className={style.linkFooter}>
          <Row>
            <Col>
              <h2>Product</h2>
              <ul className={style.products}>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    TRADE2GOV
                  </a>
                </li>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    TRADE2PORT
                  </a>
                </li>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    TRADE2SCM
                  </a>
                </li>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    TRADE2FINANCE
                  </a>
                </li>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    BPO (BUSINESS PROCESS OUTSOURCHING)
                  </a>
                </li>
              </ul>
            </Col>
            <Col>
              <h2>Company</h2>
              <ul className={style.products}>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    About
                  </a>
                </li>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    Careers
                  </a>
                </li>
              </ul>
            </Col>
            <Col>
              <h2>Support</h2>
              <ul className={style.products}>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    Help Center
                  </a>
                </li>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    FAQ
                  </a>
                </li>
                <li className={style.linkFooterLI}>
                  <a className={style.linkFooterA} href="http://">
                    Contact Us
                  </a>
                </li>
              </ul>
            </Col>
            <Col xs={5} className={style.cobaFooter}>
              <p>Database Recruitment System</p>
              <div className={style.availApps}>
                <p>PT. Edi Indonesia</p>
              </div>
            </Col>
          </Row>
          <Row className={style.lineFooter}></Row>
          <Row>
            <Col></Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
