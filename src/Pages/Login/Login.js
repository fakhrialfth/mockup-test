import React, { useEffect } from "react";
import { Formik } from "formik";
import style from "../../Styling/Pages/Login/FormLogin.module.css";
import Logo from "../../Assets/Logos/Logo.png";
import Right from "../../Assets/Icons/right blue.png";
import { useDispatch, useSelector } from "react-redux";
import { userLogin } from "../../Redux/Action/UserAction";
import { useNavigate } from "react-router";


export default function Login() {
  const error = useSelector((state) => state.getAuthRegister.error);
  const messageSuccess = useSelector((state) => state.getProfile.messageSuccess);
  const messageFail = useSelector((state) => state.getProfile.error);
  console.log(messageFail);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (messageFail || messageSuccess) {
      alert(`${messageSuccess || messageFail}`);
    }
  }, [messageFail, messageSuccess]);

  return (
    <div>
      <nav className={style.nav}>
        <a href="/">
          <img className={style.thisLogo} src={Logo} alt="Logo" />
        </a>
        <a href="/register">
          <button className={style.navButton} type="button">
            Sign up <img src={Right} alt="Right" />
          </button>
        </a>
      </nav>
      <div className={style.containerLogin}>
        <Formik
          initialValues={{ email: "", password: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = "Format Email Salah";
            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
              errors.email = "Format Email Salah";
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            dispatch(userLogin(values))
              .then((res) => {
                console.log(res);
                if (res.status === 200 && res?.data?.result?.token) {
                  navigate("/");
                }
              })
              .finally(() => {
                setSubmitting(false);
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit} className={style.formSubmit}>
              <div className={style.text}>
                <h1>Sign in</h1>
              </div>

              <input className={style.formInput} type="email" name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} placeholder="Email" />
              {errors.email && touched.email && errors.email}
              <input className={style.formInput} type="password" name="password" onChange={handleChange} onBlur={handleBlur} value={values.password} placeholder="Password" />
              <p className={style.alertError}>{(errors.password && touched.password && errors.password) || error}</p>

              <button type="submit" className={style.buttonSubmit} disabled={isSubmitting}>
                Submit
              </button>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
}
