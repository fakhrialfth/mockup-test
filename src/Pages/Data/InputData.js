import NavbarIsLogin from "../../Components/ShareComponent/NavbarIsLogin";
import style from "../../Styling/Pages/Data/Data.module.css";

export default function InputData() {
  return (
    <>
      <NavbarIsLogin />
      <div className={style.container}>
        <p>Input your data Here.</p>
      </div>
    </>
  );
}
