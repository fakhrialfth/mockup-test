import React from "react";
import { Formik } from "formik";
import style from "../../Styling/Pages/Register/FormRegister.module.css";
import Logo from "../../Assets/Logos/Logo.png";
import Right from "../../Assets/Icons/right blue.png";
import { useDispatch, useSelector } from "react-redux";
import { getUserRegister } from "../../Redux/Action/UserAction";
import { useNavigate } from "react-router";
export default function Register() {
  const error = useSelector((state) => state.getAuthRegister.error);
  console.log(error);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  return (
    <div>
      <nav className={style.nav}>
        <a href="/">
          <img className={style.thisLogos} src={Logo} alt="Logo" />
        </a>
        <a href="/login">
          <button className={style.navButton} type="button">
            Sign in <img src={Right} alt="Right" />
          </button>
        </a>
      </nav>
      <div className={style.containerLogin}>
        <Formik
          initialValues={{ email: "", password: "", name: "", acceptTerm: false }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = "Yang bener apa";
            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
              errors.email = "Invalid email address";
            }
            if (!values.acceptTerm) {
              errors.acceptTerm = "you have to agree with our T&C";
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            delete values.acceptTerm;
            console.log(values);
            dispatch(getUserRegister(values))
              .then((res) => {
                if (res.status === 201 && res?.data?.token) {
                  alert("Please Check your Email");
                  navigate("/login");
                }
              })
              .finally(() => {
                setSubmitting(false);
              });
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit} className={style.formSubmit}>
              <div className={style.text}>
                <h1>Get started</h1>
              </div>
              <input className={style.formInput} type="name" name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} placeholder="Name" />
              {errors.name && touched.name && errors.name}
              <input className={style.formInput} type="email" name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} placeholder="Email" />
              <p className={style.alertError}>{(errors.email && touched.email && errors.email) || error}</p>
              <input className={style.formInput} type="password" name="password" onChange={handleChange} onBlur={handleBlur} value={values.password} placeholder="Password min 8 characters" />
              {errors.password && touched.password && errors.password}
              <div className={style.checkboxTerms}>
                <input className={style.checkboxInput} type="checkbox" id="agreeTerms" name="acceptTerm" checked={values.acceptTerm} onChange={handleChange} />
                <label for="agreeTerms">I agree with terms & conditions</label>
              </div>
              {errors.acceptTerm}
              <button type="submit" className={style.buttonSubmit} disabled={isSubmitting}>
                Submit
              </button>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
}
